"""Module to test parser.py"""
from unittest.mock import MagicMock

from fw_gear_ants_dbm_cross_sectional.parser import parse_config


def test_parse_config(mocker, example_cross_sectional_config, example_gear_config):
    gear_context = MagicMock()

    gear_context.config_json = example_gear_config
    unzip_archive_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.parser.zip_tools.unzip_archive"
    )
    gen_temp_file_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.parser.generate_template_file_mapping"
    )
    gen_temp_file_mock.return_value = {
        "t1_registration_template": "/flywheel/v0/template/reg_template.nii.gz"
    }
    actual_template_file_mapping, actual_gear_config = parse_config(
        gear_context, "subj-01_sess01_antsCL_"
    )

    assert actual_gear_config == example_cross_sectional_config
    assert actual_template_file_mapping == {
        "t1_registration_template": "/flywheel/v0/template/reg_template.nii.gz"
    }
