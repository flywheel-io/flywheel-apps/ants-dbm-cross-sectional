import os

import pytest
from nibabel.testing import data_path

from fw_gear_ants_dbm_cross_sectional.workflow import (
    config_segmentation_workflow,
    setup_cortical_thickness,
)


def test_setup_cortical_thickness(
    example_cross_sectional_config, template_files_generator
):
    example_template_file_mapping, _ = template_files_generator
    anat_img = os.path.join(data_path, "example_nifti2.nii.gz")
    actual_node = setup_cortical_thickness(
        anat_img, example_template_file_mapping, example_cross_sectional_config
    )
    assert actual_node.interface.inputs.anatomical_image
    assert actual_node.interface.inputs.brain_probability_mask
    assert actual_node.interface.inputs.brain_template
    assert actual_node.interface.inputs.segmentation_priors
    assert actual_node.interface.inputs.t1_registration_template


def test_config_segmentation_workflow(
    example_cross_sectional_config, template_files_generator, tmp_path, caplog
):
    caplog.set_level(0)
    example_template_file_mapping, _ = template_files_generator
    anat_img = os.path.join(data_path, "example_nifti2.nii.gz")
    cortical_thickness_node = setup_cortical_thickness(
        anat_img, example_template_file_mapping, example_cross_sectional_config
    )
    work_dir = tmp_path / "work"
    sinker_dir = tmp_path / "sinker"
    actual_seg_wf = config_segmentation_workflow(
        work_dir, cortical_thickness_node, sinker_dir
    )
    assert actual_seg_wf
    recs = [
        rec
        for rec in caplog.record_tuples
        if rec[0] == "fw_gear_ants_dbm_cross_sectional.workflow"
    ]
    assert recs[0][2] == "Setting up segmentation workflow"
