"""Module to test main.py"""
import os
from pathlib import Path
from unittest.mock import MagicMock

import nipype.pipeline.engine as pe
import pytest
from nibabel.testing import data_path

from fw_gear_ants_dbm_cross_sectional.main import run


def test_run(mocker, tmpdir, example_cross_sectional_config):
    config_wf_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.main.config_segmentation_workflow"
    )
    cortical_thickness_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.main.setup_cortical_thickness"
    )
    zip_output_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.main.zip_outputs_files"
    )

    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_output_dir = Path(tmpdir.mkdir("output"))
    anat_img = os.path.join(data_path, "example_nifti2.nii.gz")

    run(example_cross_sectional_config, mock_work_dir, anat_img, {}, mock_output_dir)
    cortical_thickness_mock.assert_called_once_with(
        anat_img, {}, example_cross_sectional_config
    )
    config_wf_mock.assert_called_once_with(
        mock_work_dir, cortical_thickness_mock.return_value, mock_work_dir / "sinker"
    )
    zip_output_mock.assert_called_once_with(
        mock_work_dir / "sinker" / "outputs",
        mock_output_dir / f"{example_cross_sectional_config['out_prefix']}_outputs.zip",
    )


def test_run_w_exception(example_cross_sectional_config, tmpdir, mocker, caplog):
    config_wf_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.main.config_segmentation_workflow"
    )
    cortical_thickness_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.main.setup_cortical_thickness"
    )
    zip_output_mock = mocker.patch(
        "fw_gear_ants_dbm_cross_sectional.main.zip_outputs_files"
    )

    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_output_dir = Path(tmpdir.mkdir("output"))
    anat_img = os.path.join(data_path, "example_nifti2.nii.gz")

    workflow_mock = MagicMock(pe.Workflow(name="antsCorticalThickness"))
    workflow_mock.run.side_effect = Exception()

    config_wf_mock.return_value = workflow_mock

    with pytest.raises(SystemExit):
        run(
            example_cross_sectional_config, mock_work_dir, anat_img, {}, mock_output_dir
        )
    caplog.set_level(0)
    assert "antsCorticalThickness workflow failed..." in caplog.messages
