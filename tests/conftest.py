import pytest

from fw_gear_ants_dbm_cross_sectional import utils


@pytest.fixture
def example_cross_sectional_config():
    return {
        "atropos_iteration": 5,
        "dimension": 3,
        "b_spline_smoothing": True,
        "use_floatingpoint_precision": False,
        "segmentation_iterations": 3,
        "prior_segmentation_weight": 0.25,
        "max_iterations": "100x100x70x20",
        "denoise_anatomical_images": 1,
        "out_prefix": "subj-01_sess01_antsCL_",
    }


@pytest.fixture
def example_gear_config():
    return {
        "inputs": {
            "anatomical_image": {
                "hierarchy": {"id": "61701aabfbfaba2ac22e0af3", "type": "acquisition"},
                "object": {
                    "type": "nifti",
                    "mimetype": "application/octet-stream",
                    "classification": {},
                    "tags": [],
                    "info": {},
                    "size": 44211152,
                    "version": 1,
                    "file_id": "61702140af0d9b1c452df665",
                    "origin": {},
                },
                "location": {
                    "path": "/flywheel/v0/input/anatomical_image/t1p.nii.gz",
                    "name": "t1p.nii.gz",
                },
                "base": "file",
            }
        },
        "config": {
            "atlases_template": "OASIS-30_Atropos_template",
            "atropos_iteration": 5,
            "debug": True,
            "dimension": 3,
            "b_spline_smoothing": True,
            "use_floatingpoint_precision": False,
            "segmentation_iterations": 3,
            "prior_segmentation_weight": 0.25,
            "max_iterations": "100x100x70x20",
            "denoise_anatomical_images": 1,
        },
    }


@pytest.fixture
def template_files_generator(tmp_path):
    template_dir_01 = tmp_path / "example_template"
    for i in utils.TEMPLATE_DIR_BASE.keys():
        temp_dir = template_dir_01 / i
        temp_dir.mkdir(exist_ok=True, parents=True)
        if i == "Priors2":
            template_priors_1 = temp_dir / "priors1.nii.gz"
            template_priors_2 = temp_dir / "priors2.nii.gz"
            template_priors_1.touch()
            template_priors_2.touch()
        else:
            template_abc = temp_dir / "templatesabc.nii.gz"
            template_abc.touch()

    return {
        "Priors2": [
            f"{template_dir_01}/Priors2/priors1.nii.gz",
            f"{template_dir_01}/Priors2/priors2.nii.gz",
        ],
        "brain_extraction_probability_mask": f"{template_dir_01}/brain_extraction_probability_mask/templatesabc.nii.gz",
        "brain_segmentation_template": f"{template_dir_01}/brain_segmentation_template/templatesabc.nii.gz",
        "extraction_registration_mask": f"{template_dir_01}/extraction_registration_mask/templatesabc.nii.gz",
        "t1_registration_template": f"{template_dir_01}/t1_registration_template/templatesabc.nii.gz",
    }, template_dir_01
