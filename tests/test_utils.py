from pathlib import Path, PosixPath
from unittest import mock
from unittest.mock import MagicMock, Mock

import flywheel
import numpy as np
import pytest
from flywheel_gear_toolkit import GearToolkitContext
from nibabel.testing import data_path

from fw_gear_ants_dbm_cross_sectional import utils


def test_generate_template_file_mapping(template_files_generator):
    expected_template_file_mapping, template_dir_name = template_files_generator

    actual_template_file_mapping = utils.generate_template_file_mapping(
        Path(template_dir_name)
    )

    assert expected_template_file_mapping == actual_template_file_mapping


def test_zip_outputs_files(mocker, tmp_path):

    zip_mock = mocker.patch("fw_gear_ants_dbm_cross_sectional.utils.zipfile")

    mock_out_dir = tmp_path / "out_dir"
    mock_out_dir.mkdir(exist_ok=True, parents=True)
    test_file = mock_out_dir / "text1234.txt"
    test_file.touch()
    mock_dest_dir = tmp_path / "destination"
    mock_dest_dir.mkdir(exist_ok=True, parents=True)
    utils.zip_outputs_files(mock_out_dir, mock_dest_dir / "output.zip")
    zip_mock.ZipFile.assert_called_once_with(
        mock_dest_dir / "output.zip", "w", zip_mock.ZIP_DEFLATED
    )


def test_zip_outputs_files_w_exception():

    with pytest.raises(FileNotFoundError):
        utils.zip_outputs_files("/tmp/test_path", "/output/example.zip")
