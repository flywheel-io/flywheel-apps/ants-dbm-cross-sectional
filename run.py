#!/usr/bin/env python
"""The run script"""
import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_ants_dbm_cross_sectional.main import run
from fw_gear_ants_dbm_cross_sectional.parser import parse_config

logger = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""
    # Check the level of the run
    parent_cont = context.get_destination_parent()

    if parent_cont.container_type != "session":
        logger.error(
            f"This gear must be run at the session container not {parent_cont.container_type} level."
        )
        sys.exit(1)
    sess_label = parent_cont.label
    subj_label = context.client.get_subject(parent_cont.parents.get("subject")).label
    out_file_prefix = f"{subj_label}_{sess_label}_antsCL"
    atlases_template_file_mapping, interface_config = parse_config(
        context, out_file_prefix
    )

    run(
        interface_config,
        context.work_dir,
        context.get_input_path("anatomical_image"),
        atlases_template_file_mapping,
        context.output_dir,
    )


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        main(gear_context)
